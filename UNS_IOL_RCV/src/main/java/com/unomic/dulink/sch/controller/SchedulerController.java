package com.unomic.dulink.sch.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.sch.domain.IolVo;
import com.unomic.dulink.sch.service.SchService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/sch")
@Controller
public class SchedulerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	@Autowired
	private SchService schService;
	
	private final Boolean SET_OFF_SCH = false;
//	private final Boolean SET_OFF_SCH = true;

	@Scheduled(fixedDelay = 10000)
	public void schIol2(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_2;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol3(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_3;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol4(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_4;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol5(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_5;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol9(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_9;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol10(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_10;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol18(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_18;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol19(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_19;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol28(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_28;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol43(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_43;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol31(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_31;
		IolVo rtnVo = new IolVo();
		try {
			rtnVo = schService.getIOLData(URL);
		}catch (Exception e) {
			e.printStackTrace();
		}
		//LOGGER.info("rtnVo:"+rtnVo);
	}

	@Scheduled(fixedDelay = 10000)
	public void schIol32(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_32;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}

	@Scheduled(fixedDelay = 10000)
	public void schIol33(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_33;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol34(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_34;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@RequestMapping(value = "test34")
	@ResponseBody
	public String schIol34_test(){
		//if(SET_OFF_SCH) return null;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_34;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
		return "OK";
	}

	@Scheduled(fixedDelay = 10000)
	public void schIol35(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_35;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol40(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_40;
		IolVo rtnVo = schService.getIOLData(URL);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000) 
	public void schIol29(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_29;
		IolVo rtnVo = schService.getIOLData(URL);
//		LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol70(){
		if(SET_OFF_SCH) return;
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+CommonCode.MSG_IOL_ID_70;
		IolVo rtnVo = schService.getIOLData(URL);
//		LOGGER.info("URL:"+URL);
	}
}

