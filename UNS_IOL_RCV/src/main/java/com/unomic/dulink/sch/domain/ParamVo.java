package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class ParamVo{
	private String dvcId;
	private String date;
	private String dateTime;
	private String stDate;
	private String edDate;
	private String tgDate;
	private String name;
	private String type;
	private String ip;
}
