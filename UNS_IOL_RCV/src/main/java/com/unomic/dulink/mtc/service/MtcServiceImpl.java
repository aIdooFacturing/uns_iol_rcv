package com.unomic.dulink.mtc.service;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.mtc.domain.MtcCtrlVo;

@Service
@Repository
public class MtcServiceImpl implements MtcService{

	private final static String DEVICE_SPACE= "com.factory911.device.";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MtcServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
//	@Override
//	@Transactional(value="txManager_ma")
//	public String setChgDvcCtrl(MtcCtrlVo inputVo)
//	{
//		
//		int cntDvc = (int) sql_ma.selectOne(DEVICE_SPACE + "cntCtrlChg", inputVo);
//		if(cntDvc > 0){
//			sql_ma.update(DEVICE_SPACE + "editCtrlChg", inputVo);	
//		}else{
//			sql_ma.insert(DEVICE_SPACE + "addCtrlChg", inputVo);
//		}
//
//		return "OK";
//	}

}
