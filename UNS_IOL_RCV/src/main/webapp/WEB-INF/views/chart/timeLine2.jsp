<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/custom_evt.js"></script> 
<script type="text/javascript">
var selected_bg = "rgb(115,230, 147)";
var selected_txt_color = "white";
$(function () {
	setEl();
	bindEvt();
});

var openMenu = false;
function bindEvt(){
	$("#menuBox").click(togglePanel);
	
	$("#timeBox span").bind("mouseover", function(evt){
		var selected = $(this).hasClass("selected");
		
		if(!selected){
			$(this).css({
				"background-color" : "white",
				"color" : selected_bg
			});			
		};
	});
	
	$("#timeBox span").bind("mouseout", function(evt){
		var selected = $(this).hasClass("selected");
		
		if(!selected){
			$(this).css({
				"background-color" : selected_bg,
				"color" : "white"
			});			
		};
	});
	
	$("#timeBox span").click(function(){
		$("#timeBox span").removeClass("selected");
		$("#timeBox span").css({
			"background-color" : selected_bg,
			"color" : "white"
		});
		
		$(this).addClass("selected").css({
			"background-color" : "white",
			"color" : selected_bg
		});
		
		openMenu = true;
		togglePanel();	
		$("#leftBox").animate({
			"left" : getElSize(100),
			"opacity" : 1
		});
		
		$("#rightBox").animate({
			"top" : (window.innerHeight/2) - ($("#rightBox").height()/2),
			"opacity" : 1
		});
	});
};

function togglePanel(){
	if(!openMenu){
		$("#timeBox").animate({
			"top" : 0
		});
		$("#menuBox").animate({
			"top" : $("#timeBox").height() + getElSize(60)
		});
		
		$("#leftBox").animate({
			"left" : -$("#leftBox").width(),
			"opacity" : 0
		});
		
		$("#rightBox").animate({
			"top" : -$("#rightBox").height(),
			"opacity" : 0
		});
	}else{
		$("#timeBox").animate({
			"top" : -$("#timeBox").height() - getElSize(60)
		});
		$("#menuBox").animate({
			"top" : 0
		});
	};
	openMenu = !openMenu;
};

function setEl(){
	var width = window.innerWidth;
	var height = window.innerHeight;
	
	$("#content").css({
		"width" : width,
		"height" : height,
		"background-image" : "url(" +  ctxPath + "/images/bg_note.jpg)",
		"background-size" : "100% 100%",
	});
	
	$("#corver").css({
		"width" : width,
		"height" : height,
		"position" : "absolute",
		"background-color" : "rgba(0,0,0,0.3)"
	});
	
	$("#menuBox").css({
		"width" : getElSize(100),
		"cursor" : "pointer",
		"position" : "absolute"
	});
	
	$("#menuBox div").css({
		"height" : getElSize(10),
		"border-bottom" : getElSize(15) + "px solid white",
		"width" : "100%",
	});
	
	$("#menuBox").css({
		"left" : (width/2) - ($("#menuBox").width()/2) 
	});
	
	$("#timeBox span").css({
		"font-size" : getElSize(80),
		"font-weight" : "bolder",
		"padding" : getElSize(30),
		"cursor" : "pointer",
		"color" : "white"
	});
	
	$("#timeBox").css({
		"position" : "absolute",
		"width" : width,
		"padding" : getElSize(30),
		"top" : -$("#timeBox").height() - getElSize(60),
		"background-color" : "-moz-linear-gradient(left, rgba(114,230,147,1) 0%, rgba(114,230,147,1) 3%, rgba(145,232,66,1) 44%, rgba(145,232,66,1) 100%)"
	});
	
	$("#leftBox").css({
		"width" : width * 0.7,
		"height" : height * 0.9,
		"background-color" : "-moz-linear-gradient(left, rgba(114,230,147,1) 0%, rgba(114,230,147,1) 3%, rgba(145,232,66,1) 44%, rgba(145,232,66,1) 100%)",
		"position" : "absolute",
		"opacity" : 0
		
	});
	
	$("#rightBox").css({
		"width" : width * 0.23,
		"height" : height * 0.9,
		"background-color" : "-moz-linear-gradient(left, rgba(114,230,147,1) 0%, rgba(114,230,147,1) 3%, rgba(145,232,66,1) 44%, rgba(145,232,66,1) 100%)",
		"position" : "absolute",
		"opacity" : 0
	});
	
	$("#rightBox").css({
		"top" : -$("#rightBox").height(),
		"right" : getElSize(100)
	});
	
	$("#leftBox").css({
		"top" : (height/2) - ($("#leftBox").height()/2),
		"left" : -$("#leftBox").width()
	});
};

</script>
<style type="text/css">
	body{
		overflow: hidden;
	}
	*{
		margin: 0px;
		padding: 0px;
	}
</style>
</head>
<body>
	<div id="content">
		<div id="corver"></div>
		<div id="timeBox">
			<center>
				<span>1</span>
				<span>2</span>
				<span>3</span>
				<span>4</span>
				<span>5</span>
				<span>6</span>
				<span>7</span>
				<span>8</span>
				<span>9</span>
				<span>10</span>
				<span>11</span>
				<span>12</span>
				<span>13</span>
				<span>14</span>
				<span>15</span>
				<span>16</span>
				<span>17</span>
				<span>18</span>
				<span>19</span>
				<span>20</span>
				<span>21</span>
				<span>22</span>
				<span>23</span>
				<span>24</span>
			</center>
		</div>
		
		<div id="menuBox">
			<div id="fLine"></div>
			<div id="sLine"></div>
			<div id="lLine"></div>
		</div>
		
		<div id="leftBox"></div>
		<div id="rightBox"></div>
	</div>
</body>
</html>