package com.unomic.dulink.sch.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sch.domain.IolVo;

@Service
@Repository
public class SchServiceImpl implements SchService{

	private final static String SCH_SPACE= "com.unos.sch.";
	private final static String IOL_SPACE= "com.unos.iol.";
	
	private final static Logger LOGGER = LoggerFactory.getLogger(SchServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	
	/*
	 * 필요 json 양식.
	 * dvcId 
	 * iologik. 0001,0010 등.
	 * */
	@Override
	public IolVo getIOLData(String URL)
	{
		IolVo inputVo = setJsonData(URL);
		//ReturnVo inputVo =  new Gson().fromJson(strJson,ReturnVo.class);
		//LOGGER.info("inputVo:"+inputVo);
		
//		inputVo.setStatus("0001");
//		LOGGER.info("inputVo:"+inputVo.getStatus());
		
		
		//LOGGER.info("STEP1");
		
		if(inputVo.getStartDateTime()==null){
			setAlarm(inputVo);	// ioLojik 알람 추가
			inputVo.setEx1("Ex1-Connection Closed");
			return inputVo;
		}

		inputVo = getIOLStatus(inputVo);

		setAlarm(inputVo);	// ioLojik 알람 추가
		
		//System.out.println("inputVo:"+inputVo);
		if(getCntIOL(inputVo)!=1){
			//LOGGER.info("STEP2");
			inputVo.setIsExist(false);
			return inputVo;
		}

		//기존쿼리 문제없을지 확인하기.
		//마지막 상태 가져오기.
		// dvcId만 있으면 정상 작동.
		IolVo preIolVo = getLastInputData(inputVo);
		IolVo startVo;
		
		//LOGGER.info("STEP3");
		//LOGGER.info("preIolVo:"+preIolVo);
		
		if(preIolVo == null){
			//LOGGER.info("STEP_PRE_NULL");
			preIolVo = new IolVo();
			//preIolVo.setDvcId(Integer.valueOf(inputVo.getDvcId()));
			preIolVo.setDvcId(inputVo.getDvcId());
			preIolVo.setStartDateTime(CommonCode.MSG_INIT_DT);
			
			startVo = chkDateStarterIOL(preIolVo, inputVo);
			
			addIOLStatus(inputVo);
			inputVo.setIsSuccess(true);
			
			return inputVo;
		}else{
			//LOGGER.info("STEP4");
			//LOGGER.info("preIolVo:"+preIolVo);
			//LOGGER.info("iolVo:"+inputVo);
			
			preIolVo = chkIOLStatus(preIolVo);	
	
			// dvcId만 있으면 됨.
			//이전 데이터와 지금 시간 비교해서 dateStater input.
			startVo = chkDateStarterIOL(preIolVo, inputVo);
			
			//dvcId, startDateTime
			//editLastEndTime(inputVo);
			////LOGGER.info("Before Duple:"+preIOLVo.getEndDateTime());
			//dvcId,startDateTime, status 필요함.
			
			try {
				sql_ma.update(IOL_SPACE+"updateLastData",inputVo);
			}catch(Exception e) {
				e.printStackTrace();
			}
			
			if(null==isDupleIOL(preIolVo, inputVo)
					&& null == preIolVo.getEndDateTime())
			{//check Duplication.
				//LOGGER.info("STEP_DUPLE");
				inputVo.setIsDuple(true);
				addDupleDvc(inputVo);
				return inputVo;
			}else{
				//데이터 스타터 들어갔을 경우에 처리. lastEndDateTime을 datestater시간으로 넣어줘야함.
				if(null != startVo){
					//LOGGER.info("STEP_STARTER");
					inputVo.setStartDateTime(startVo.getEndDateTime());
				}
				
				//#{dvcId}, #{startDateTime}, #{status}, #{chartStatus}, #{workDate}
				//dvcId, startDateTime
				
				
				editLastEndTime(inputVo);
				if(inputVo.getDvcId().equals("34")) {
					/*System.out.println(inputVo);*/
				}
				addIOLStatus(inputVo);
				inputVo.setIsSuccess(true);
			}
			
			
			
			return inputVo;
		}
	}
	
//	@Override
//	public void getIOLData()
//	{
//		List<IolVo> listAdt = getListIOL();
//		int size = listAdt.size();
//		//LOGGER.info("number of IOL:"+size);
//		IolVo tmpVo = new IolVo();
//		for(int i = 0 ; i<size ;i++){
//			try {
//				tmpVo = listAdt.get(i);
//				
//				URL url = new URL(CommonCode.MSG_HTTP + tmpVo.getIp() + CommonCode.MSG_IOL_URL);
//				URLConnection con = url.openConnection();
//				con.setConnectTimeout(CommonCode.CONNECT_TIMEOUT);
//				con.setReadTimeout(CommonCode.READ_TIMEOUT);
//				InputStream in = con.getInputStream();
//				
//				BufferedReader br = new BufferedReader(new InputStreamReader(in));
//				String strTemp = org.apache.commons.io.IOUtils.toString(br);
//				IolVo pureVo = getIOLStatus(strTemp,tmpVo.getDvcId());
//
//				DeviceVo setVo = new DeviceVo();
//				setVo.setDvcId(tmpVo.getDvcId());
//				setVo.setLastUpdateTime(pureVo.getStartDateTime());
//				setVo.setLastChartStatus(pureVo.getChartStatus());
//				setVo.setLastStartDateTime(pureVo.getStartDateTime());
//				setVo.setWorkDate(CommonFunction.mil2WorkDate(System.currentTimeMillis()));
//
//				//last 상태 update 함.
//				editLastStatusIOL(setVo);
//
//				//마지막 상태 가져오기.
//				IolVo preIOLVo = getLastInputData(pureVo);
//				preIOLVo = chkIOLStatus(preIOLVo);
//				
//				IolVo startVo = chkDateStarterIOL(preIOLVo, pureVo);
//				
//				////LOGGER.info("Before Duple:"+preIOLVo.getEndDateTime());
//				if(null==isDupleIOL(preIOLVo, pureVo)
//						&& null == preIOLVo.getEndDateTime()
//					){//check Duplication.
//					////LOGGER.info("RUN nothing");
//				}else{
//					if(null != startVo){
//						pureVo.setStartDateTime(startVo.getEndDateTime());;
//					}
//					
//					editLastEndTime(pureVo);
//					addIOLStatus(pureVo);
//				}
//				
//			}catch(SocketTimeoutException ex) {
//				//LOGGER.error("[ADT_ID:"+tmpVo.getDvcId()+"@@]"+"IOL NO-CONNECTION:"+"[IP:"+ tmpVo.getIp()+"]");
//			}catch(UnknownHostException exx){
//				//LOGGER.error("[ADT_ID:"+tmpVo.getDvcId()+"@@]"+"UnknownHostException Error");
//			}catch(Exception e){
//				//LOGGER.error("[ADT_ID:"+tmpVo.getDvcId()+"@@]"+"IOL Error");
//				e.printStackTrace();
//			} 
//		}
//		
//	}
	
//	private List<IolVo> getListIOL()
//	{
//		List<IolVo> rtnList  = sql_ma.selectList(IOL_SPACE + "getListIOL");
//		return rtnList;
//	}

	private void setAlarm(IolVo inputVo) {
		
	if(inputVo.getStartDateTime()==null){
		// 알람 테이블에 end_time이 null이 있는지 확인 
		int isEmpty = (int) sql_ma.selectOne(IOL_SPACE + "getLastAlarm", inputVo.getDvcId()); 
							
		if(isEmpty > 0) {
//			LOGGER.info("업데이트 : " + inputVo.getDvcId());
			sql_ma.update(IOL_SPACE + "updateEndDateTime", inputVo.getDvcId());
		}
		
		return;
	}
		
	// 현재 값이 'ALARM'인 경우 
		if(inputVo.getChartStatus().equals("ALARM")) {
			
			// 알람 테이블에 end_time이 null이 있는지 확인 
			int isEmpty = (int) sql_ma.selectOne(IOL_SPACE + "getLastAlarm", inputVo.getDvcId()); 
								
			if(isEmpty == 0) {
//				LOGGER.info(inputVo.getDvcId() + " : ALARM INSERT");									
				sql_ma.insert(IOL_SPACE+"addAlarmData", inputVo);				
			}
			
		}else {	// 현재 값이 'ALARM'이 아닌 경우 

			// 알람 테이블에 end_time이 null이 있는지 확인 
			int isEmpty = (int) sql_ma.selectOne(IOL_SPACE + "getLastAlarm", inputVo.getDvcId()); 
								
			if(isEmpty > 0) {
//				LOGGER.info("업데이트 : " + inputVo.getDvcId());
				sql_ma.update(IOL_SPACE + "updateEndDateTime", inputVo.getDvcId());
			}
		}
	}
	
	private IolVo getIpIOL(IolVo inputVo)
	{
		IolVo rtnVo = (IolVo) sql_ma.selectOne(IOL_SPACE + "getIpIOL",inputVo);
		
		return rtnVo;
	}
	
	private String addDupleDvc(IolVo inputVo)
	{
		sql_ma.insert(IOL_SPACE + "addDupleDvc",inputVo);
		
		return "OK";
	}
	
	
	private int getCntIOL(IolVo inputVo)
	{
		int rtnCnt = (int) sql_ma.selectOne(IOL_SPACE + "getCntIOL",inputVo);
		
		return rtnCnt;
	}
	
	private IolVo getIOLStatus(IolVo inputVo){
		
		//IolVo pureVo = new IolVo();
		String dvcId = inputVo.getDvcId();
		char[] array={'0','0','0','0'};
		array = inputVo.getStatus().toCharArray();
		//pureVo.setDvcId(Integer.valueOf(dvcId));
		//pureVo.setDvcId(dvcId);
		
		if(dvcId.equals("34")||dvcId.equals("35")||dvcId.equals("40")){
			
			inputVo.setIoPower((array[0] == '1') ? true : false );
		    inputVo.setIoInCycle((array[1] == '1') ? true : false );
		    inputVo.setIoWait((array[2] == '1') ? true : false );
		    inputVo.setIoAlarm((array[3] == '1') ? true : false );
		}else{// origin.
			inputVo.setIoPower((array[0] == '1') ? true : false );
		    inputVo.setIoInCycle((array[1] == '1') ? true : false );
		    inputVo.setIoAlarm((array[2] == '1') ? true : false );
		    inputVo.setIoWait((array[3] == '1') ? true : false );
		}
	    
	    inputVo.setStatus(""+array[0]+array[1]+array[2]+array[3]);
	    
	    if(inputVo.getIoAlarm()){
	    	inputVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if (inputVo.getIoWait()){
	    	inputVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else if(inputVo.getIoInCycle()){
	    	inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }else{
	    	inputVo.setChartStatus(CommonCode.MSG_WAIT);
	    }
	    
	    long crntMillTime = System.currentTimeMillis();
	    
	    inputVo.setStartDateTime(CommonFunction.unixTime2Datetime(crntMillTime));
	    inputVo.setWorkDate(CommonFunction.mil2WorkDate(crntMillTime));
	    
	    
		return inputVo;
	}
	
//	private IolVo getIOLStatus(String getResult,String dvcId){
//		IolVo pureVo = new IolVo();
//		
//		String[] array;
//		array = getResult.split("=|\\<");
//		//pureVo.setDvcId(Integer.valueOf(dvcId));
//		pureVo.setDvcId(dvcId);
//		
//		if(dvcId.equals("34")||dvcId.equals("35")||dvcId.equals("40")){
//			pureVo.setIoPower((null != array[1] && array[1].equals("1")) ? true : false );
//		    pureVo.setIoInCycle((null != array[3] && array[3].equals("1")) ? true : false );
//		    pureVo.setIoWait((null != array[5] && array[5].equals("1")) ? true : false );
//		    pureVo.setIoAlarm((null != array[7] && array[7].equals("1")) ? true : false );
//		}else{// origin.
//			pureVo.setIoPower((null != array[1] && array[1].equals("1")) ? true : false );
//		    pureVo.setIoInCycle((null != array[3] && array[3].equals("1")) ? true : false );
//		    pureVo.setIoAlarm((null != array[5] && array[5].equals("1")) ? true : false );
//		    pureVo.setIoWait((null != array[7] && array[7].equals("1")) ? true : false );
//		}
//	    
//	    pureVo.setStatus(array[1]+array[3]+array[5]+array[7]);
//	    
//	    if(pureVo.getIoAlarm()){
//	    	pureVo.setChartStatus(CommonCode.MSG_ALARM);
//	    }else if (pureVo.getIoWait()){
//	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
//	    }else if(pureVo.getIoInCycle()){
//	    	pureVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
//	    }else{
//	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
//	    }
//	    
//	    long crntMillTime = System.currentTimeMillis();
//	    
//	    pureVo.setStartDateTime(CommonFunction.unixTime2Datetime(crntMillTime));
//	    pureVo.setWorkDate(CommonFunction.mil2WorkDate(crntMillTime));
//	    
//		return pureVo;
//	}
	
	//dvcId만 있으면 됨.
	private IolVo getLastInputData(IolVo inputVo){
		IolVo rtnVo = new IolVo();
		rtnVo.setDvcId(inputVo.getDvcId());
		
			rtnVo  = (IolVo) sql_ma.selectOne(IOL_SPACE + "getLastAdapterStatus",inputVo);
			return rtnVo;
    	
	}

	public IolVo chkIOLStatus(IolVo inputVo){
		//int dvcId = inputVo.getDvcId();
		String dvcId = inputVo.getDvcId();
		String ioStatus = inputVo.getStatus();
		if(ioStatus == null || ioStatus.length() != CommonCode.IOL_STATUS_LENGTH){
			return inputVo;
		}
		
		if (inputVo.getDvcId().equals("34") || inputVo.getDvcId().equals("35") || inputVo.getDvcId().equals("40")) {
			inputVo.setIoPower((ioStatus.charAt(0)=='1') ? true : false);
			inputVo.setIoInCycle((ioStatus.charAt(1)=='1') ? true : false);
			inputVo.setIoAlarm((ioStatus.charAt(3)=='1') ? true : false);
			inputVo.setIoWait((ioStatus.charAt(2)=='1') ? true : false);
		}else {
			inputVo.setIoPower((ioStatus.charAt(0)=='1') ? true : false);
			inputVo.setIoInCycle((ioStatus.charAt(1)=='1') ? true : false);
			inputVo.setIoAlarm((ioStatus.charAt(2)=='1') ? true : false);
			inputVo.setIoWait((ioStatus.charAt(3)=='1') ? true : false);
		}
		
		
	    if(inputVo.getIoAlarm()){
	    	inputVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if(inputVo.getIoWait()){
	    	inputVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else{
	    	inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }
		return inputVo;
	}

	public IolVo chkDateStarterIOL(IolVo preVo, IolVo crtVo){
		//LOGGER.info("Run dateStarterIOL");
		//LOGGER.info("preVo:"+preVo);
		//LOGGER.info("crtVo:"+crtVo);
		
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime()))
		{
			//LOGGER.info("STEP5");
			//LOGGER.info("@@@@@RUN DateStarter IOL@@@@@");
			//LOGGER.info("startDateTime:"+preVo.getStartDateTime());
			//LOGGER.info("endDateTime:"+crtVo.getStartDateTime());
			
			IolVo starterVo = new IolVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setEndDateTime(CommonFunction.getStandardP1SecToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			//list.add(i, starterVo);
			if(preVo != null){
				//LOGGER.info("STEP6");
				editLastEndTime(starterVo);
			}
			addIOLStatus(starterVo);
	
			return starterVo;
		}
		
		return null;
	}

	
	
	@Transactional
	private String editLastEndTime(IolVo firstOfListVo)
	{
		//LOGGER.info("editLastEndTime");
		/*int cnt = (int) sql_ma.selectOne(IOL_SPACE + "cntAdapterStatus", firstOfListVo);
		if(cnt < 1){
			return "OK";
		}*/
		
		IolVo tmpAdapterVo = (IolVo) sql_ma.selectOne(IOL_SPACE + "getLastStartTime", firstOfListVo);
		if(tmpAdapterVo != null){
			sql_ma.delete(IOL_SPACE + "rmExceptionEndTime",tmpAdapterVo);
		}
		tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
		sql_ma.update(IOL_SPACE + "editLastEndTime", tmpAdapterVo);
		
		return "OK";
	}
	
	@Transactional
	private String addIOLStatus(IolVo pureStatusVo)
	{
		sql_ma.insert(IOL_SPACE+"addIOLData", pureStatusVo);
		return "OK";
	}
	
	public IolVo isDupleIOL(IolVo preIOLVo, IolVo inputVo){
		Long preStartTime = 0L;
		if(null == preIOLVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preIOLVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		if(crtStartTime < preStartTime){
			//LOGGER.error("timeTrash data");
			return null;
		}
		
		//LOGGER.info("preIOLVo.getStatus():" + preIOLVo.getStatus());
		//LOGGER.info("inputVo.getStatus():" + inputVo.getStatus());
    	if( preIOLVo.getStatus() != null && preIOLVo.getStatus().equals(inputVo.getStatus())){
    		
    	    	return null;
    	}else{
    		return inputVo;
    	}
	}
	
	private IolVo setJsonData(String url){
		String strStep = "0";
		String USER_AGENT = "Mozilla/5.0";
		
		IolVo inputVo = new IolVo();
		
		URL obj;
		int responseCode =0;
		
		String rtnJson="";
		strStep += "1";
		try {
			obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			
			//Gson gson = new Gson();
			//String strJson = gson.toJson(inputVo);
			
			//inputVo.setStrJson(strJson);
			//wr.writeBytes(strJson);
			wr.writeBytes("");
			wr.flush();
			wr.close();
			responseCode = con.getResponseCode();
			inputVo.setRspnsCode(responseCode);
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			rtnJson = response.toString();
			//LOGGER.info("rtnJson:"+rtnJson);
			inputVo =  new Gson().fromJson(rtnJson,IolVo.class);
			
			//LOGGER.info("inputVo:"+inputVo);
			
		}catch(Exception e){
			e.printStackTrace();
		}

		return inputVo;
	}
	
}