//170621 Work start

package com.unomic.dulink.mtc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sch.domain.IolVo;
import com.unomic.dulink.sch.service.SchService;

/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/mtc")
@Controller
public class MtcController {
	
	private static final Logger logger = LoggerFactory.getLogger(MtcController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@Autowired
	private SchService schService;
	
	@RequestMapping(value = "getInitTime")
	@ResponseBody
    public String setInitTime(){
		return CommonFunction.getTodayDateTime();
	}
	
	@RequestMapping(value = "timeTest")
	@ResponseBody
    public String timeTest(){
		return CommonFunction.getTodayDateTime();
	}
	
//	@RequestMapping(value="rcvIol")
//	@ResponseBody
//	public ReturnVo rcvIol(@RequestBody String strJson){
//		logger.info("strJson:"+strJson);
//		ReturnVo rtnVo = schService.getIOLDataTest(strJson);
//		
//		return rtnVo;
//	}
	
	@RequestMapping(value="getIol")
	@ResponseBody
	public IolVo getIol(String dvcId){
		String URL="http://mtmes.doosanmachinetools.com/as/sch/getIol.do?key=";
		URL = URL+dvcId;
		
		IolVo rtnVo = schService.getIOLData(URL);
		
		return rtnVo;
	}
	
}
